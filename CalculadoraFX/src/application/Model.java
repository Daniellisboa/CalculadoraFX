package application;

public class Model {
	
	public float calculate (long n1, long n2, String operacao) {
			
			switch (operacao) {
			case "+":
				return n1 + n2;
			case "-":
				return n1 - n2;
			case "*":
				return n1 * n2;
			case "/":
				if(n2 == 0)
					return 0;
				else 
					return n1 / n2;
			default:
				return 0;
			}
	}
}
